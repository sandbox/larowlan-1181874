<?php
// $Id$
/**
 * @file
 * TNT administration menu items.
 */

/**
 * TNT Quote settings.
 *
 * Record TNT account information neccessary to use the service.
 * Configure which TNT services are quoted to customers.
 *
 * @ingroup forms
 * @see uc_admin_settings_validate()
 */
function uc_tnt_admin_settings() {
  $form = array();

  $form['uc_tnt_conditions'] = array(
    '#value' => '<p>'. l(t('Set the conditions that will return a TNT quote.'), CA_UI_PATH .'/uc_tnt_get_quote/edit/conditions') .'</p>',
  );

  $form['uc_tnt_account_number'] = array('#type' => 'textfield',
    '#title' => t('TNT Account number'),
    '#description' => t('This can be found in the TNT Express Price Checker'),
    '#default_value' => variable_get('uc_tnt_account_number', ''),
    '#required' => TRUE,
  );
  $form['uc_tnt_user_id'] = array('#type' => 'textfield',
    '#title' => t('TNTExpress.com.au user ID'),
    '#default_value' => variable_get('uc_tnt_user_id', ''),
    '#required' => TRUE,
  );
  $form['uc_tnt_password'] = array('#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('uc_tnt_password', ''),
  );
  $form['uc_tnt_services'] = array('#type' => 'checkboxes',
    '#title' => t('TNT services'),
    '#default_value' => variable_get('uc_tnt_services', _uc_tnt_service_list()),
    '#options' => _uc_tnt_service_list(),
    '#description' => t('Select the TNT services that are available to customers.'),
  );
  $form['uc_tnt_markup_type'] = array('#type' => 'select',
    '#title' => t('Markup type'),
    '#default_value' => variable_get('uc_tnt_markup_type', 'percentage'),
    '#options' => array(
      'percentage' => t('Percentage (%)'),
      'multiplier' => t('Multiplier (×)'),
      'currency' => t('Addition (!currency)', array('!currency' => variable_get('uc_currency_sign', '$'))),
    ),
  );
  $form['uc_tnt_markup'] = array('#type' => 'textfield',
    '#title' => t('Shipping rate markup'),
    '#default_value' => variable_get('uc_tnt_markup', '0'),
    '#description' => t('Markup TNT shipping rate quote by currency amount, percentage, or multiplier.'),
  );
  
  $form['#validate'][] = 'uc_tnt_admin_settings_validate';

  return system_settings_form($form);
}

/**
 * Validation handler for uc_tnt_admin_settings.
 *
 * Require password only if it hasn't been set.
 *
 * @see uc_tnt_admin_settings()
 */
function uc_tnt_admin_settings_validate($form, &$form_state) {
  $old_password = variable_get('uc_tnt_password', '');
  if (!$form_state['values']['uc_tnt_password']) {
    if ($old_password) {
      form_set_value($form['uc_tnt_password'], $old_password, $form_state);
    }
    else {
      form_set_error('uc_tnt_password', t('Password field is required.'));
    }
  }
}

/**
 * Theme function to format the TNT product name and rate amount line-item shown
 *   to the customer.
 *
 * @param $service
 *   The TNT product/service name.
 * @ingroup themeable
 */
function theme_uc_tnt_option_label($service) {
  // Start with logo as required by the tnt terms of service.
  $output = '<img class="tnt-logo" src="'. url(drupal_get_path('module', 'uc_tnt')) .'/uc_tnt_logo.gif" alt="'. t('TNT Logo') .'" /> ';

  // Add the TNT service name.
  $output .= t('TNT @service', array('@service' => $service));

  return $output;
}
